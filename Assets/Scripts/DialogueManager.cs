using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Pipes;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using TMPro;


    public class DialogueManager : MonoBehaviour
    {

        StarterInput starterAssets;
        public NPC npc;

        bool isTalking = false;

        float distance;
        float curResponseTracker;

        public GameObject player;
        public GameObject dialogueUI;
        public GameObject OtherdialogueUI1;
        public GameObject OtherdialogueUI2;
        public GameObject CCam;

        public TMP_Text npcName;
        public TMP_Text npcDialogueBox;
        public TMP_Text playerResponse1;
        public TMP_Text playerResponse2;
        public Button playerResponseButton1;
        public Button playerResponseButton2;

       //and its back
        private void Awake()
        {
            starterAssets = new StarterInput();
        }

        private void OnEnable()
        {
            starterAssets.Enable();
        }

        private void OnDisable()
        {
            starterAssets.Disable();
        }
        //finaly the end of spaghetti

        // Start is called before the first frame update
        void Start()
        {
            dialogueUI.SetActive(false);
            OtherdialogueUI1.SetActive(false);
            OtherdialogueUI2.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        void OnMouseOver()
        {
            distance = Vector3.Distance(player.transform.position, this.transform.position);
            if(distance <= 2.5f)
            {
                //trigger dialogue
                if((starterAssets.Player.DialogueInteract.triggered) && isTalking==false)
                {
                    StartConversation();
                }
                else if((starterAssets.Player.DialogueInteract.triggered) && isTalking==true)
                {
                    EndDialogue();
                }
            }
        }
        void StartConversation()
        {
            isTalking = true;
            curResponseTracker = 0;
            dialogueUI.SetActive(true);
            npcName.text = npc.name;
            npcDialogueBox.text = npc.dialogue[0];
            playerResponse1.text = npc.playerDialogue[0];
            playerResponse2.text = npc.playerDialogue[1];
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            CCam.SetActive(false);
        }
        void EndDialogue()
        {
            isTalking = false;
            dialogueUI.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            CCam.SetActive(true);
        }
    }
