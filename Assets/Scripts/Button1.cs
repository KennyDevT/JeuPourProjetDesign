using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Pipes;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using TMPro;

public class Button1 : MonoBehaviour
{
    public TMP_Text npcDialogueBox;

    public NPC npc;

    public void ButtonClicked()
    {
        npcDialogueBox.text = npc.dialogue[1];
    }
}
